/// <reference types="q" />
import * as q from 'q';
import { INpmpageConfig } from '../npmpage.options';
export declare let run: (configArg: INpmpageConfig) => q.Promise<{}>;
