import { LazyModule } from 'smartsystem';
import * as _modStandard from './mod_standard/index';
import * as _modPublish from './mod_publish/index';
export declare let modStandard: LazyModule<typeof _modStandard>;
export declare let modPublish: LazyModule<typeof _modPublish>;
