export declare let packageDir: string;
export declare let webJsBundleFile: string;
export declare let cwd: string;
export declare let docsDir: string;
export declare let pagesDir: string;
export declare let publicDir: string;
export declare let coverageDir: string;
