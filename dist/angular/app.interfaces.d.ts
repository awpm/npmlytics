export interface IMenuItemObject {
    title: string;
    relPath: string;
    content: string;
    project: {
        name: string;
    };
}
